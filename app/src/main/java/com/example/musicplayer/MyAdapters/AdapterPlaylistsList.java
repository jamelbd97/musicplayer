package com.example.musicplayer.MyAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.musicplayer.MyObjects.Playlist;
import com.example.musicplayer.R;

import java.util.ArrayList;

public class AdapterPlaylistsList extends BaseAdapter {

    private ArrayList<Playlist> playlists;
    private Context c;

    public AdapterPlaylistsList(Context c, ArrayList<Playlist> playlists) {
        this.c = c;
        this.playlists = playlists;
    }

    @Override
    public int getCount() {
        return playlists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View listView;

        if (convertView == null) {
            listView = inflater.inflate(R.layout.for_adapter_playlists, null);
        } else {
            listView = convertView;
        }

        listView.setTag(position);

        TextView playlistName = listView.findViewById(R.id.playlist_name);
//        ImageButton playlistDelete = listView.findViewById(R.id.playlist_delete_button);

        Playlist currPlaylist = playlists.get(position);

        playlistName.setText(currPlaylist.getPlaylistName());

        return listView;
    }
}
package com.example.musicplayer.MyAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musicplayer.MyObjects.Album;
import com.example.musicplayer.R;

import java.util.ArrayList;

public class AdapterAlbumList extends BaseAdapter {

    private ArrayList<Album> albums;
    private Context c;

    public AdapterAlbumList(Context c, ArrayList<Album> albums) {
        this.c = c;
        this.albums = albums;
    }

    @Override
    public int getCount() {
        return albums.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        if (convertView == null) {
            gridView = inflater.inflate(R.layout.for_adapter_album, null);
        } else {
            gridView = convertView;
        }

        gridView.setTag(position);

        ImageView albumArt = gridView.findViewById(R.id.albumArt);
        TextView albumName = gridView.findViewById(R.id.albumName);
        TextView artistName = gridView.findViewById(R.id.artistName);

        Album currAlbum = albums.get(position);

        albumArt.setImageBitmap(currAlbum.getAlbumArt());
        albumArt.setScaleType(ImageView.ScaleType.CENTER_CROP);

        albumName.setText(currAlbum.getAlbumName());
        artistName.setText(currAlbum.getAlbumArtist());

        return gridView;
    }
}
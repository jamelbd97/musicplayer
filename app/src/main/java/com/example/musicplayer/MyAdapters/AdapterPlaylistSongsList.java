package com.example.musicplayer.MyAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.musicplayer.MyObjects.Song;
import com.example.musicplayer.R;

import java.util.ArrayList;

public class AdapterPlaylistSongsList extends BaseAdapter implements ListAdapter {

    public static final String Broadcast_REFRESH_UI = "com.example.musicplayer.RefreshUi";
    private ArrayList<Song> songs;
    private Context context;

    public AdapterPlaylistSongsList(Context context , ArrayList<Song> songs) {
        this.songs = songs;
        this.context = context;
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int pos) {
        return songs.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.for_adapter_playlists, null);
        }

        TextView title = view.findViewById(R.id.playlist_popup_music_title);
        TextView artist = view.findViewById(R.id.playlist_popup_music_artist);

        Song currPlaylistSong = songs.get(position);

        title.setText(currPlaylistSong.getTitle());
        artist.setText(currPlaylistSong.getArtist());

        ImageButton delete = view.findViewById(R.id.playlist_popup_delete_button);

        delete.setOnClickListener((View v) ->{
            // TODO Auto-generated method stub
            //storage.loadPlaylist()
            Toast.makeText(context, "song removed from playlist", Toast.LENGTH_SHORT).show();
            //Intent in = new Intent(Broadcast_REFRESH_UI_POPUP);
            //context.sendBroadcast(in);
        });

        title.setOnClickListener((View v) ->{

        });

        return view;
    }
}
package com.example.musicplayer.MyAdapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.musicplayer.MyObjects.Song;
import com.example.musicplayer.R;

import java.util.ArrayList;

public class AdapterMusicList extends BaseAdapter {

    private ArrayList<Song> songs;
    private LayoutInflater songInf;

    public AdapterMusicList(Context c, ArrayList<Song> theSongs) {
        songs = theSongs;
        songInf = LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //map to for_adapter_song layout
        LinearLayout songLay = (LinearLayout) songInf.inflate(R.layout.for_adapter_song, parent, false);
        //get title and artist views
        TextView songView = songLay.findViewById(R.id.song_title);
        TextView artistView = songLay.findViewById(R.id.song_artist);
        //get for_adapter_song using position
        Song currSong = songs.get(position);
        //get title and artist strings
        songView.setText(currSong.getTitle());
        songView.setTextSize(20);
        songView.setTypeface(null, Typeface.NORMAL);
        artistView.setText(currSong.getArtist());
        artistView.setTextSize(16);
        artistView.setTextColor(Color.DKGRAY);
        //set position as tag
        songLay.setTag(position);
        return songLay;
    }
}
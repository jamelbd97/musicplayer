package com.example.musicplayer.MyObjects;

public class Song {
    private int songId;
    private String data;
    private String title;
    private String album;
    private long albumId;
    private String artist;

    public Song(int songId, String data, String title, String album, long albumId, String artist) {
        this.songId = songId;
        this.data = data;
        this.title = title;
        this.album = album;
        this.albumId = albumId;
        this.artist = artist;
    }

    public int getSongId() {
        return songId;
    }

    public String getData() {
        return data;
    }

    public String getTitle() {
        return title;
    }

    public long getAlbumId() {
        return albumId;
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }
}
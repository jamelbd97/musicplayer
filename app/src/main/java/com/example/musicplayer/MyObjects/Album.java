package com.example.musicplayer.MyObjects;

import android.graphics.Bitmap;

public class Album {

    private long albumId;
    private String albumName;
    private String albumArtist;
    private Bitmap albumArt;

    public Album(long albumId, String albumName, String albumArtist, Bitmap albumArt) {
        this.albumId = albumId;
        this.albumName = albumName;
        this.albumArtist = albumArtist;
        this.albumArt = albumArt;
    }

    public long getAlbumId() {
        return albumId;
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getAlbumArtist() {
        return albumArtist;
    }

    public Bitmap getAlbumArt() {
        return albumArt;
    }
}

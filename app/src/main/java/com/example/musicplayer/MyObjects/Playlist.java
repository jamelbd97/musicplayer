package com.example.musicplayer.MyObjects;

public class Playlist {

    private long playlistId;
    private String playlistName;

    public Playlist(long playlistId, String playlistName) {
        this.playlistId = playlistId;
        this.playlistName = playlistName;
    }

    public long getPlaylistId() {
        return playlistId;
    }

    public String getPlaylistName() {
        return playlistName;
    }
}
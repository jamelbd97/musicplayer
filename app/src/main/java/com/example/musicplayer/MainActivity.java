package com.example.musicplayer;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.musicplayer.MyObjects.Song;
import com.example.musicplayer.mediaPlayer.MediaPlayerService;
import com.example.musicplayer.mediaPlayer.StorageUtil;
import com.example.musicplayer.ui.music.AlbumsFragment;
import com.example.musicplayer.ui.music.MusicFragment;
import com.example.musicplayer.ui.music.PlaylistsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    public static final String Broadcast_PLAY_NEW_AUDIO = "com.example.musicplayer.PlayNewAudio";
    public static final String Broadcast_UPDATE_ON_SEEK = "com.example.musicplayer.UpdateOnSeek";

    public final String MUSIC_FRAGMENT = "music_fragment";
    public final String ALBUMS_FRAGMENT = "albums_fragment";
    public final String PLAYLISTS_FRAGMENT = "playlists_fragment";

    boolean threadRunning;

    boolean serviceBound = false;
    int repeat;
    boolean shuffleState;
    private BottomNavigationView.OnNavigationItemSelectedListener navListener = (@NonNull MenuItem item) -> {
        switch (item.getItemId()) {
            case R.id.bottom_navigation_music:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new MusicFragment(), "music_fragment").commit();
                break;
            case R.id.bottom_navigation_albums:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new AlbumsFragment(), "albums_fragment").commit();
                break;
            case R.id.bottom_navigation_playlists:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new PlaylistsFragment(), "playlists_fragment").commit();
                break;
        }
        return true;
    };
    private BottomSheetBehavior bottomSheetBehavior;
    private LinearLayout linearLayoutBSheet;
    private MediaPlayerService player;
    private ImageButton buttonPlayPause;
    private ImageButton buttonPlayPause2;
    private BottomNavigationView bottomNavigationBar;
    private Handler mHandler = new Handler();
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;
            linearLayoutBSheet.setVisibility(View.VISIBLE);
            updateNowPlayingScreen();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };
    private BroadcastReceiver closeService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (serviceBound) {
                unbindService(serviceConnection);
                serviceBound = false;
                Log.w("MainActivity", "Service is now unbound");
            }
            linearLayoutBSheet.setVisibility(View.INVISIBLE);
        }
    };
    private BroadcastReceiver updateNowPlayingScreenBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isPlaying = Objects.requireNonNull(intent.getExtras()).getBoolean("playing");
            boolean updateOnlyButton = Objects.requireNonNull(intent.getExtras()).getBoolean("updateOnlyPlayButton");
            if (!updateOnlyButton) {
                updateNowPlayingScreen();
            }
            if (isPlaying) {
                buttonPlayPause.setBackgroundResource(R.drawable.np_pause);
                buttonPlayPause2.setImageResource(R.drawable.np_pause);
            } else {
                buttonPlayPause.setBackgroundResource(R.drawable.np_play);
                buttonPlayPause2.setImageResource(R.drawable.np_play);
            }
            if ((!isPlaying) && (!updateOnlyButton)) {
                linearLayoutBSheet.setVisibility(View.INVISIBLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        if (isPermissionGranted()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MusicFragment(), "music_fragment").commit();
        } else {
            askForPermissions();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (isPermissionGranted()) {
            onResume();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MusicFragment(), "music_fragment").commit();
        } else {
            ((ActivityManager) (Objects.requireNonNull(getSystemService(Context.ACTIVITY_SERVICE)))).clearApplicationUserData();
            recreate();
        }
    }

    public boolean isPermissionGranted() {
        boolean storagePermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED;
        boolean phonePermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED;
        return (!storagePermission && !phonePermission);
    }

    public void askForPermissions() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE}, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isPermissionGranted()) {
            return;
        }

        init();

        if (isMyServiceRunning()) {
            if (!serviceBound) {
                Intent playerIntent = new Intent(this, MediaPlayerService.class);
                Log.w("MainActivity", "Resuming .. service not bound trying to bind ..");
                bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
            }
        }

        if (player != null) {
            if (player.isMediaPlaying()) {
                linearLayoutBSheet.setVisibility(View.VISIBLE);
            }
        }

        Intent intent = getIntent();
        if (intent.getIntExtra("index", -1) >= 0) {
            ArrayList<String> pathList = intent.getStringArrayListExtra("music_list");
            int index = intent.getIntExtra("index", -1);
            if (pathList != null) {
                if (pathList.size() > 0) {
                    playSelectedPlaylist(index, getSongsFromPath(pathList));
                }
            }
        }
    }

    private ArrayList<Song> getSongsFromPath(ArrayList<String> newPathList) {
        ContentResolver contentResolver = Objects.requireNonNull(getApplicationContext()).getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        ArrayList<Song> newList = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0) {
            int i = 0;
            while (cursor.moveToNext()) {
                int audioId = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioList
                if (newPathList.get(i).equals(data)) {
                    newList.add(new Song(audioId, data, title, album, albumId, artist));
                }
                if (i < newPathList.size() - 1) {
                    i++;
                }
            }
        }
        assert cursor != null;
        cursor.close();
        return newList;
    }

    public void init() {
        registerNowPlayingScreenBroadcast();
        registerCloseServiceBroadcast();

        threadRunning = false;

        repeat = 2; //  0 for don't repeat / 1 for repeat all / 2 for repeat one song
        linearLayoutBSheet = findViewById(R.id.nowPlayingBottomSheet);
        bottomSheetBehavior = BottomSheetBehavior.from(linearLayoutBSheet);
        buttonPlayPause = findViewById(R.id.nowPlayingButtonPlayPause);
        buttonPlayPause2 = findViewById(R.id.nowPlayingButtonPlayPauseBottom);
        bottomNavigationBar = findViewById(R.id.bottom_navigation);

        ((ToggleButton) findViewById(R.id.nowPlayingArrow)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            boolean toggle = false;

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (checkFragmentActive()) {
                    if (newState == BottomSheetBehavior.STATE_EXPANDED && !toggle) {
                        TranslateAnimation animation = new TranslateAnimation(0, 0, 0, bottomNavigationBar.getHeight());
                        animation.setDuration(400);
                        bottomNavigationBar.startAnimation(animation);
                        bottomNavigationBar.setVisibility(View.GONE);

                        findViewById(R.id.nowPlayingButtonPrev).setVisibility(View.GONE);
                        findViewById(R.id.nowPlayingButtonPlayPause).setVisibility(View.GONE);
                        findViewById(R.id.nowPlayingButtonNext).setVisibility(View.GONE);
                        toggle = true;

                    } else if (newState == BottomSheetBehavior.STATE_COLLAPSED && toggle) {
                        bottomNavigationBar.setVisibility(View.VISIBLE);
                        TranslateAnimation animation = new TranslateAnimation(0, 0, bottomNavigationBar.getHeight(), 0);
                        animation.setDuration(400);
                        bottomNavigationBar.startAnimation(animation);

                        findViewById(R.id.nowPlayingButtonPrev).setVisibility(View.VISIBLE);
                        findViewById(R.id.nowPlayingButtonPlayPause).setVisibility(View.VISIBLE);
                        findViewById(R.id.nowPlayingButtonNext).setVisibility(View.VISIBLE);

                        toggle = false;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                findViewById(R.id.nowPlayingArrow).setRotation(slideOffset * 180);
            }
        });

        bottomNavigationBar.setOnNavigationItemSelectedListener(navListener);
    }

    public boolean checkFragmentActive() {
        MusicFragment testFrag = (MusicFragment) getSupportFragmentManager().findFragmentByTag(MUSIC_FRAGMENT);
        AlbumsFragment testFrag1 = (AlbumsFragment) getSupportFragmentManager().findFragmentByTag(ALBUMS_FRAGMENT);
        PlaylistsFragment testFrag2 = (PlaylistsFragment) getSupportFragmentManager().findFragmentByTag(PLAYLISTS_FRAGMENT);

        boolean test = false, test1 = false, test2 = false;

        if (testFrag != null) {
            test = testFrag.isVisible();
        }
        if (testFrag1 != null) {
            test1 = testFrag1.isVisible();
        }
        if (testFrag2 != null) {
            test2 = testFrag2.isVisible();
        }

        return (test || test1 || test2);
    }

    public void playSelectedPlaylist(int audioIndex, ArrayList<Song> list) {
        //Check is service is active
        if (!serviceBound) {
            Log.w("MainActivity", "Service not bound trying to bind ..");
            //Store Serializable audioList to SharedPreferences

            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudio(list);
            storage.storeAudioIndex(audioIndex);

            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            //Store the new audioIndex to SharedPreferences
            StorageUtil storage = new StorageUtil(getApplicationContext());
            storage.storeAudio(list);
            storage.storeAudioIndex(audioIndex);

            //Send a broadcast to the service -> PLAY_NEW_AUDIO
            Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
            sendBroadcast(broadcastIntent);
        }
    }

    public void playButtonClick(View v) {
        if (player.isMediaPlaying()) {
            player.pauseMedia();
        } else {
            player.resumeMedia();
        }
    }

    public void prevButtonClick(View v) {
        player.skipToPrevious();
    }

    public void nextButtonClick(View v) {
        player.skipToNext();
    }

    public void repeatButtonClick(View v) {
        ImageButton repeatButton = findViewById(R.id.nowPlayingRepeatMusic);
        switch (repeat) {
            case 0:
                repeatButton.setColorFilter(Color.argb(255, 200, 200, 200));
                repeatButton.setImageResource(R.drawable.np_repeat);
                player.setRepeatState(0);
                repeat = 1;
                break;
            case 1:
                repeatButton.setColorFilter(Color.DKGRAY);
                repeatButton.setImageResource(R.drawable.np_repeat);
                player.setRepeatState(1);
                repeat = 2;
                break;
            case 2:
                repeatButton.setColorFilter(Color.DKGRAY);
                repeatButton.setImageResource(R.drawable.np_repeat_one);
                player.setRepeatState(2);
                repeat = 0;
                break;
        }
    }

    public void shuffleButtonClick(View v) {
        ImageButton shuffleButton = findViewById(R.id.nowPlayingPlayRandomly);

        MusicFragment fragment = (MusicFragment) getSupportFragmentManager().findFragmentByTag(MUSIC_FRAGMENT);
        if (shuffleState) {
            ArrayList<Song> orderedPlaylist = fragment.getOrderedPlaylist();
            playSelectedPlaylist(0, orderedPlaylist);
            shuffleButton.setColorFilter(Color.argb(255, 200, 200, 200));
            shuffleState = false;
        } else {
            ArrayList<Song> shuffledPlaylist = fragment.createShuffledPlaylist();
            playSelectedPlaylist(0, shuffledPlaylist);
            shuffleButton.setColorFilter(Color.DKGRAY);
            shuffleState = true;
        }


    }

    public void updateNowPlayingScreen() {
        if (player == null) {
            return;
        }

        Song activeSong = player.getActiveAudio();

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(activeSong.getData());

        ((TextView) findViewById(R.id.nowPlayingTitleLabel)).setText(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
        (findViewById(R.id.nowPlayingTitleLabel)).setSelected(true);

        ((TextView) findViewById(R.id.nowPlayingArtistLabel)).setText(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));

        int duration = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

        ((TextView) findViewById(R.id.nowPlayingMusicDuration)).setText(convertDuration(duration));

        updateSeekBar(findViewById(R.id.progress_bar), duration);

        updateAlbumArt(findViewById(R.id.nowPlayingAlbumArtBig), activeSong.getAlbumId());

        buttonPlayPause.setBackgroundResource(R.drawable.np_pause);
        buttonPlayPause2.setImageResource(R.drawable.np_pause);
    }

    public void updateAlbumArt(ImageView albumArt, long albumId) {
        albumArt.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (getSongAlbumArt(albumId) != null) {
            albumArt.setImageBitmap(getSongAlbumArt(albumId));
        } else {
            Bitmap noArt = BitmapFactory.decodeResource(getResources(), R.drawable.no_art);
            albumArt.setImageBitmap(noArt);
        }
    }

    public Bitmap getSongAlbumArt(long albumId) {
        Bitmap albumArt = null;
        try {
            final Uri AlbumArtUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(AlbumArtUri, albumId);
            ParcelFileDescriptor pfd = getContentResolver().openFileDescriptor(uri, "r");

            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                albumArt = BitmapFactory.decodeFileDescriptor(fd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return albumArt;
    }

    public void updateSeekBar(SeekBar seekBar, int duration) {
        TextView progressText = findViewById(R.id.nowPlayingMusicProgress);
        progressText.setText(convertDuration(0));

        seekBar.setMax(duration);
        seekBar.setProgress(0);

        if (!threadRunning) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (serviceBound) {
                        if (player.isMediaPlaying()) {
                            threadRunning = true;
                            if (player != null) {
                                int currentPosition = player.getCurrentPosition();
                                seekBar.setProgress(currentPosition);
                                progressText.setText(convertDuration(player.getCurrentPosition()));
                            }
                        }
                        mHandler.postDelayed(this, 1000);
                    }
                }
            });
        }
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                ((TextView) findViewById(R.id.nowPlayingMusicProgress))
                        .setText(convertDuration(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Intent broadcastIntent = new Intent(Broadcast_UPDATE_ON_SEEK);
                broadcastIntent.putExtra("newPos", seekBar.getProgress());
                sendBroadcast(broadcastIntent);
            }
        });
    }

    public String convertDuration(long duration) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) - minutes * 60;

        if (seconds < 10) {
            return minutes + ":0" + seconds;
        } else {
            return minutes + ":" + seconds;
        }
    }

    public void registerNowPlayingScreenBroadcast() {
        IntentFilter filter = new IntentFilter(MediaPlayerService.Broadcast_UPDATE_METADATA);
        registerReceiver(updateNowPlayingScreenBroadcast, filter);
    }

    public void registerCloseServiceBroadcast() {
        IntentFilter filter = new IntentFilter(MediaPlayerService.Broadcast_CLOSE_SERVICE);
        registerReceiver(closeService, filter);
    }

    public void hideBottomBar(boolean hide) {
        if (hide) {
            TranslateAnimation animation = new TranslateAnimation(0, 0, 0, bottomNavigationBar.getHeight());
            animation.setDuration(400);
            bottomNavigationBar.startAnimation(animation);
            bottomNavigationBar.setVisibility(View.GONE);
        } else {
            bottomNavigationBar.setVisibility(View.VISIBLE);
            TranslateAnimation animation = new TranslateAnimation(0, 0, bottomNavigationBar.getHeight(), 0);
            animation.setDuration(400);
            bottomNavigationBar.startAnimation(animation);
        }
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MediaPlayerService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("ServiceState", serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("ServiceState");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(updateNowPlayingScreenBroadcast);
        unregisterReceiver(closeService);

        if (serviceBound) {
            unbindService(serviceConnection);
            serviceBound = false;
            //service is active
            player.stopSelf();
        }
    }
}
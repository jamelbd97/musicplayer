package com.example.musicplayer.ui.music;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.musicplayer.MainActivity;
import com.example.musicplayer.MyAdapters.AdapterPlaylistsList;
import com.example.musicplayer.MyObjects.Playlist;
import com.example.musicplayer.R;
import com.example.musicplayer.ui.music.popups.PlaylistPopup;

import java.util.ArrayList;
import java.util.Objects;

public class PlaylistsFragment extends Fragment {

    private ArrayList<Playlist> playlistList;
    private boolean isInit;
    private View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_playlists, container, false);

        init();

        // On Click add a playlist
        Button createPlaylist = view.findViewById(R.id.createPlaylist);
        createPlaylist.setOnClickListener((final View v) -> {
            addPlaylistDialog();
        });

        return view;
    }

    private void init() {
        loadPlaylistList();
        displayPlaylistList();
        isInit = true;
    }

    private void refreshView() {
        Fragment fragment = null;
        fragment = getActivity().getSupportFragmentManager().findFragmentByTag("playlists_fragment");
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.detach(fragment);
        ft.attach(fragment);
        ft.commit();
    }

    private void loadPlaylistList() {
        playlistList = new ArrayList<>();

        ContentResolver resolver = getActivity().getContentResolver();

        Log.w("PLAYLISTS", "Looking for playlists list");

        Uri playlistsUri = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        String[] cols = new String[]{"*"};

        Cursor cursor = resolver.query(playlistsUri, cols, null, null, null);

        assert cursor != null;
        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            long playlistId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Playlists._ID));
            String playlistName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists.NAME));

            playlistList.add(new Playlist(playlistId, playlistName));
        }
        cursor.close();

        if (playlistList.size() < 1) {
            Log.w("PLAYLISTS", "You have no playlists");
        }
    }

    private void displayPlaylistList() {
        if (playlistList != null) {
            if (playlistList.size() < 1) {
                createNotFoundText(view);
            } else {
                ListView playlistsListView = view.findViewById(R.id.playlistList);

                AdapterPlaylistsList playlistAdapter = new AdapterPlaylistsList(getContext(), playlistList);
                playlistsListView.setVisibility(View.VISIBLE);
                playlistsListView.setAdapter(playlistAdapter);

                playlistsListView.setOnItemClickListener((AdapterView<?> arg0, View arg1, int position, long arg3) -> {
                    createPlaylistPopup(position);
                });

                playlistsListView.setOnItemLongClickListener((AdapterView<?> arg0, View arg1, int position, long arg3) -> {
                    deletePlaylistDialog(position);
                    return true;
                });
            }
        } else {
            createNotFoundText(view);
        }
    }

    private void createNotFoundText(View view) {
        TextView text = new TextView(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);

        text.setLayoutParams(params);
        text.setText(R.string.noPlaylists);
        text.setTextSize(25);
        text.setTextColor(Color.DKGRAY);
        text.setGravity(Gravity.CENTER);
        ((LinearLayout) view.findViewById(R.id.linearListLayoutPlaylists)).addView(text);
    }

    private void deletePlaylist(Long playlistId) {
        ContentResolver contentResolver = getActivity().getContentResolver();
        // // Log.i(TAG, "deletePlaylist");
        String id = playlistId + "";
        String where = MediaStore.Audio.Playlists._ID + "=?";
        String[] whereVal = {id};
        contentResolver.delete(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, where, whereVal);
    }

    private void addPlaylistDialog() {
        Dialog dialog = new Dialog(Objects.requireNonNull(getContext()));
        dialog.setContentView(R.layout.dialog_add_playlist);
        dialog.show();

        EditText editText = dialog.findViewById(R.id.edit_name);

        dialog.findViewById(R.id.dialog_add_button).setOnClickListener((View view) -> {
            String playlistName = editText.getText().toString();
            if (playlistName.equals("")) {
                Toast.makeText(getContext(), "Playlist name can't be empty", Toast.LENGTH_LONG).show();
            } else {
                createPlaylist(playlistName);
                Toast.makeText(getContext(), "Playlist added", Toast.LENGTH_LONG).show();
                refreshView();
                dialog.cancel();
            }
        });
        dialog.findViewById(R.id.dialog_cancel_button).setOnClickListener((View view) -> {
            dialog.cancel();
        });
    }

    private void deletePlaylistDialog(int position) {
        Dialog dialog = new Dialog(Objects.requireNonNull(getContext()));
        dialog.setContentView(R.layout.dialog_delete_playlist);
        dialog.show();

        long playlistId = playlistList.get(position).getPlaylistId();
        String playlistName = playlistList.get(position).getPlaylistName();

        TextView editText = dialog.findViewById(R.id.dialog_delete_text);
        editText.setText("Delete : " + playlistName + " ?");

        dialog.findViewById(R.id.dialog_delete_delete_button).setOnClickListener((View view) -> {
            deletePlaylist(playlistId);
            Toast.makeText(getContext(), "Playlist deleted", Toast.LENGTH_LONG).show();
            refreshView();
            dialog.cancel();
        });
        dialog.findViewById(R.id.dialog_delete_cancel_button).setOnClickListener((View view) -> {
            dialog.cancel();
        });
    }

    private void createPlaylistPopup(int position) {
        assert playlistList != null;
        String playlistName = playlistList.get(position).getPlaylistName();
        long playlistId = playlistList.get(position).getPlaylistId();

        PlaylistPopup playlistFragmentPopup = new PlaylistPopup();
        Bundle args = new Bundle();
        args.putString("playlist_name", playlistName);
        args.putLong("playlist_id", playlistId);
        playlistFragmentPopup.setArguments(args);

        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, playlistFragmentPopup, "playlist_fragment_popup")
                .addToBackStack("playlist_fragment")
                .commit();

        ((MainActivity) getActivity()).hideBottomBar(true);
    }

    private void createPlaylist(String name) {
        ContentResolver resolver = getActivity().getContentResolver();

        Log.w("PLAYLISTS", "Checking for existing playlist for " + name);

        Uri playlistsUri = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        String[] cols = new String[]{"*"};

        Cursor cursor = resolver.query(playlistsUri, cols, null, null, null);

        long playlistId = 0;

        assert cursor != null;
        cursor.moveToFirst();
        while (cursor.moveToNext()) {
            String playlistNames = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists.NAME));
            if (playlistNames.equalsIgnoreCase(name)) {
                playlistId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Playlists._ID));
                break;
            }
        }
        cursor.close();

        if (playlistId != 0) {
            //error playlist already exitst
        }

        Log.w("PLAYLISTS", "CREATING PLAYLIST: " + name);

        ContentValues v = new ContentValues();
        v.put(MediaStore.Audio.Playlists.NAME, name);
        v.put(MediaStore.Audio.Playlists.DATE_MODIFIED, System.currentTimeMillis());
        Uri newPlaylist = resolver.insert(playlistsUri, v);

        Log.w("PLAYLISTS", "Added PlayLIst: " + newPlaylist);
        cursor.close();
    }
}
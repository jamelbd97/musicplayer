package com.example.musicplayer.ui.music.popups;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.musicplayer.MainActivity;
import com.example.musicplayer.MyAdapters.AdapterMusicList;
import com.example.musicplayer.MyObjects.Song;
import com.example.musicplayer.R;

import java.io.FileDescriptor;
import java.util.ArrayList;

public class AlbumPopup extends Fragment {

    private ArrayList<Song> musicList;
    private long albumIdFromIntent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.popup_fragment_album, container, false);

        Bundle args = getArguments();
        assert args != null;
        String albumName = args.getString("album_name");
        albumIdFromIntent = args.getLong("album_id");

        ((TextView) v.findViewById(R.id.popup_album_name)).setText(albumName);
        (v.findViewById(R.id.popup_album_name)).setSelected(true);

        Bitmap albumArt = getSongAlbumArt(albumIdFromIntent);
        ImageView albumArtView = v.findViewById(R.id.popup_album_art);
        albumArtView.setImageBitmap(albumArt);
        albumArtView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        loadAudioInAlbum();

        ListView musicListView = v.findViewById(R.id.popup_album_music_list);

        AdapterMusicList songAdapter = new AdapterMusicList(getContext(), musicList);
        musicListView.setVisibility(View.VISIBLE);
        musicListView.setAdapter(songAdapter);

        musicListView.setOnItemClickListener((parent, view, position, id) -> {
            ((MainActivity) getActivity()).playSelectedPlaylist(position, musicList);
        });


        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private Bitmap getSongAlbumArt(Long albumId) {
        Bitmap albumArt = null;
        try {
            final Uri AlbumArtUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(AlbumArtUri, albumId);
            ParcelFileDescriptor pfd = getContext().getContentResolver().openFileDescriptor(uri, "r");

            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                albumArt = BitmapFactory.decodeFileDescriptor(fd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (albumArt == null) {
            albumArt = BitmapFactory.decodeResource(getResources(), R.drawable.no_art);
        }
        return albumArt;
    }

    private void loadAudioInAlbum() {
        ContentResolver contentResolver = getContext().getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            musicList = new ArrayList<>();
            while (cursor.moveToNext()) {
                int audioId = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioList
                if (albumIdFromIntent == albumId) {
                    musicList.add(new Song(audioId, data, title, album, albumId, artist));
                }

            }
        }
        assert cursor != null;
        cursor.close();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) getActivity()).hideBottomBar(false);
    }
}
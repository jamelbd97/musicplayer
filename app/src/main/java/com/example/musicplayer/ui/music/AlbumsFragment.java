package com.example.musicplayer.ui.music;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.musicplayer.MainActivity;
import com.example.musicplayer.MyAdapters.AdapterAlbumList;
import com.example.musicplayer.MyObjects.Album;
import com.example.musicplayer.R;
import com.example.musicplayer.ui.music.popups.AlbumPopup;

import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.Objects;

public class AlbumsFragment extends Fragment {
    @Nullable
    private ArrayList<Album> albumsList;
    private boolean isInit;
    private View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_albums, container, false);
        isInit = false;

        MyTask task = new MyTask();
        task.execute();

        return view;
    }

    private void getAlbumsList() {
        //retrieve albums info
        ContentResolver musicResolver = getContext().getContentResolver();
        Uri albumsUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String sortOrder = MediaStore.Audio.Albums.ALBUM + " ASC";
        Cursor cursor = musicResolver.query(albumsUri, null, null, null, sortOrder);

        if (cursor != null && cursor.moveToFirst()) {

            //get columns
            int albumIdColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Albums._ID);
            int albumNameColumn = cursor.getColumnIndex
                    (MediaStore.Audio.Albums.ALBUM);
            int albumArtist = cursor.getColumnIndex
                    (MediaStore.Audio.Albums.ARTIST);

            //add Albums to list
            while (cursor.moveToNext()) {
                long thisAlbumId = cursor.getLong(albumIdColumn);
                String thisAlbumName = cursor.getString(albumNameColumn);
                String thisAlbumArtist = cursor.getString(albumArtist);
                Bitmap thisAlbumArt;
                if (getSongAlbumArt(cursor.getLong(albumIdColumn)) != null) {
                    thisAlbumArt = getSongAlbumArt(cursor.getLong(albumIdColumn));
                } else {
                    Bitmap noArt = BitmapFactory.decodeResource(getResources(), R.drawable.no_art);
                    thisAlbumArt = noArt;
                }

                if (albumsList != null) {
                    albumsList.add(new Album(thisAlbumId, thisAlbumName, thisAlbumArtist, thisAlbumArt));
                }
            }
            cursor.close();
        }
    }

    private void startMusicFromSelectedAlbum(int index, long albumId) {

    }

    private Bitmap getSongAlbumArt(Long albumId) {
        Bitmap albumArt = null;
        try {
            final Uri AlbumArtUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(AlbumArtUri, albumId);
            ParcelFileDescriptor pfd = getContext().getContentResolver().openFileDescriptor(uri, "r");

            if (pfd != null) {
                FileDescriptor fd = pfd.getFileDescriptor();
                albumArt = BitmapFactory.decodeFileDescriptor(fd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return albumArt;
    }

    private void init() {

        albumsList = new ArrayList<>();
        getAlbumsList();
        // On AlbumItem Click
        ((GridView) view.findViewById(R.id.albumsGrid)).setOnItemClickListener((parent, v, position, id) -> {
            createAlbumPopup(position);
        });

        isInit = true;
    }

    private void createAlbumPopup(int position) {
        assert albumsList != null;
        String albumName = albumsList.get(position).getAlbumName();
        long albumId = albumsList.get(position).getAlbumId();

        AlbumPopup albumFragmentPopup = new AlbumPopup();
        Bundle args = new Bundle();
        args.putString("album_name", albumName);
        args.putLong("album_id", albumId);
        albumFragmentPopup.setArguments(args);

        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, albumFragmentPopup, "album_fragment_popup")
                .addToBackStack("album_fragment")
                .commit();

        ((MainActivity) getActivity()).hideBottomBar(true);
    }

    private class MyTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog Asycdialog = new ProgressDialog(getContext());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Asycdialog.setMessage("Loading Albums...");
            Asycdialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            init();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
            Asycdialog.dismiss();

            GridView albumGridView = view.findViewById(R.id.albumsGrid);
            AdapterAlbumList songAdapter = new AdapterAlbumList(getContext(), albumsList);
            albumGridView.setAdapter(songAdapter);
        }
    }
}
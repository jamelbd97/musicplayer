package com.example.musicplayer.ui.music.popups;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.musicplayer.MyAdapters.AdapterMusicList;
import com.example.musicplayer.MyObjects.Song;
import com.example.musicplayer.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class SongsPopup extends Fragment {
    View view;
    @Nullable
    private ArrayList<Song> musicList;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.popup_fragment_songs, container, false);
        init(view);
        return view;
    }

    private void init(View v) {

        Bundle args = getArguments();
        assert args != null;
        long playlistId = args.getLong("playlist_id");

        loadAudio();
        if ((musicList == null) || (musicList.size() < 1)) {
            TextView noMusicText = new TextView(getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);

            noMusicText.setLayoutParams(params);
            noMusicText.setText(R.string.noMusic);
            noMusicText.setTextSize(25);
            noMusicText.setTextColor(Color.DKGRAY);
            noMusicText.setGravity(Gravity.CENTER);
            ((LinearLayout) v.findViewById(R.id.popup_fragment_linear_layout)).addView(noMusicText);
        } else {
            ListView musicListView = v.findViewById(R.id.popup_fragment_songs_list);

            AdapterMusicList songAdapter = new AdapterMusicList(getContext(), musicList);
            musicListView.setVisibility(View.VISIBLE);
            musicListView.setAdapter(songAdapter);

            musicListView.setOnItemClickListener((AdapterView<?> arg0, View arg1, int position, long arg3) -> {
                addToPlaylist(playlistId, musicList.get(position).getSongId());
                Snackbar.make(view, "Added to playlist : " + musicList.get(position).getTitle(), 2000).show();
            });

            Collections.sort(musicList, (a, b) -> a.getTitle().compareTo(b.getTitle()));
        }
    }

    private void addToPlaylist(long playlistId, int audioId) {
        ContentResolver contentResolver = getContext().getContentResolver();

        String[] cols = new String[]{
                "count(*)"
        };
        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
        Cursor cur = contentResolver.query(uri, cols, null, null, null);

        assert cur != null;
        cur.moveToFirst();
        final int base = cur.getInt(0);
        cur.close();
        ContentValues values = new ContentValues();
        values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, Integer.valueOf(base + audioId));
        values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
        contentResolver.insert(uri, values);
    }

    private void loadAudio() {
        ContentResolver contentResolver = Objects.requireNonNull(getContext()).getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            musicList = new ArrayList<>();
            while (cursor.moveToNext()) {
                int audioId = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioList
                musicList.add(new Song(audioId, data, title, album, albumId, artist));
            }
        }
        assert cursor != null;
        cursor.close();
    }
}
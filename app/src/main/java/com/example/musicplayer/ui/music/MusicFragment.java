package com.example.musicplayer.ui.music;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.musicplayer.MainActivity;
import com.example.musicplayer.MyAdapters.AdapterMusicList;
import com.example.musicplayer.MyObjects.Song;
import com.example.musicplayer.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Random;

public class MusicFragment extends Fragment {
    @Nullable
    private ArrayList<Song> musicList;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music, container, false);

        ListView songsListView = view.findViewById(R.id.musicList);

        init(view);

        songsListView.setOnItemClickListener((AdapterView<?> arg0, View arg1, int position, long arg3) -> {
            assert (getActivity()) != null;
            ((MainActivity) getActivity()).playSelectedPlaylist(position, musicList);
        });

        return view;
    }

    private void init(View v) {
        Button playRand = v.findViewById(R.id.randomPlay);

        loadAudio();

        if ((musicList == null) || (musicList.size() < 1)) {
            TextView noMusicText = new TextView(getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);

            noMusicText.setLayoutParams(params);
            noMusicText.setText(R.string.noMusic);
            noMusicText.setTextSize(25);
            noMusicText.setTextColor(Color.DKGRAY);
            noMusicText.setGravity(Gravity.CENTER);
            ((LinearLayout) v.findViewById(R.id.linearListLayout)).addView(noMusicText);
        } else {
            ListView musicListView = v.findViewById(R.id.musicList);

            AdapterMusicList songAdapter = new AdapterMusicList(getContext(), musicList);
            musicListView.setVisibility(View.VISIBLE);
            musicListView.setAdapter(songAdapter);

            Collections.sort(musicList, (a, b) -> a.getTitle().compareTo(b.getTitle()));
        }

        // On Click Music Plays Randomly
        playRand.setOnClickListener((final View view) -> {
            Toast.makeText(getContext(), "Shuffling", Toast.LENGTH_SHORT).show();
            if (musicList != null) {
                ArrayList<Song> shuffledPlaylist = createShuffledPlaylist();
                ((MainActivity) getActivity()).playSelectedPlaylist(0, shuffledPlaylist);
            }
        });
    }

    private void loadAudio() {
        ContentResolver contentResolver = Objects.requireNonNull(getContext()).getContentResolver();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            musicList = new ArrayList<>();
            while (cursor.moveToNext()) {
                int audioId = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                // Save to audioList
                musicList.add(new Song(audioId, data, title, album, albumId, artist));
            }
        }
        assert cursor != null;
        cursor.close();
    }

    public ArrayList<Song> createShuffledPlaylist() {
        if (musicList == null) {
            loadAudio();
        }
        ArrayList<Song> orderedList = new ArrayList<>(musicList);
        ArrayList<Song> newList = new ArrayList<>();
        while (orderedList.size() > 0) {
            int randomInt = new Random().nextInt(orderedList.size());
            Song song = orderedList.get(randomInt);
            newList.add(song);
            orderedList.remove(song);
        }
        return newList;
    }

    public ArrayList<Song> getOrderedPlaylist() {
        return musicList;
    }
}
package com.example.musicplayer.ui.music.popups;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.musicplayer.MainActivity;
import com.example.musicplayer.MyAdapters.AdapterMusicList;
import com.example.musicplayer.MyObjects.Song;
import com.example.musicplayer.R;

import java.util.ArrayList;
import java.util.Objects;

public class PlaylistPopup extends Fragment {

    private ArrayList<Song> musicList;
    private long playlistIdFromIntent;
    private String playlistNameFromIntent;
    private View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.popup_fragment_playlist, container, false);

        Bundle args = getArguments();
        assert args != null;
        playlistNameFromIntent = args.getString("playlist_name");
        playlistIdFromIntent = args.getLong("playlist_id");

        ((TextView) v.findViewById(R.id.popup_playlist_name)).setText(playlistNameFromIntent);
        (v.findViewById(R.id.popup_playlist_name)).setSelected(true);

        loadAudioInPlaylist(playlistIdFromIntent);

        displayPlaylistList();

        (v.findViewById(R.id.popup_add_music)).setOnClickListener((view) -> {
            createSongPopup(playlistIdFromIntent);
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void createSongPopup(long playlistId) {
        SongsPopup songsFragmentPopup = new SongsPopup();
        Bundle argsToGive = new Bundle();
        argsToGive.putLong("playlist_id", playlistId);
        songsFragmentPopup.setArguments(argsToGive);

        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, songsFragmentPopup, "playlist_songs_popup")
                .addToBackStack("playlist_fragment_popup")
                .commit();
    }

    private void loadAudioInPlaylist(long playlistId) {
        ContentResolver contentResolver = getContext().getContentResolver();
        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
        String selection = MediaStore.Audio.Playlists.Members.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Playlists.Members.TITLE + " ASC";
        Cursor cursor = contentResolver.query(uri, null, selection, null, sortOrder);

        if (cursor != null && cursor.getCount() > 0) {
            musicList = new ArrayList<>();
            while (cursor.moveToNext()) {
                int audioId = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Playlists.Members._ID));
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists.Members.DATA));
                String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists.Members.TITLE));
                String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists.Members.ALBUM));
                long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Playlists.Members.ALBUM_ID));
                String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists.Members.ARTIST));

                musicList.add(new Song(audioId, data, title, album, albumId, artist));
            }
        }
        assert cursor != null;
        cursor.close();
    }

    private void displayPlaylistList() {
        if (musicList != null) {
            if (musicList.size() > 0) {
                ListView playlistListView = v.findViewById(R.id.popup_fragment_playlist_songs_list);
                AdapterMusicList songAdapter = new AdapterMusicList(getContext(), musicList);
                playlistListView.setVisibility(View.VISIBLE);
                playlistListView.setAdapter(songAdapter);

                playlistListView.setOnItemClickListener((parent, view, position, id) -> {
                    ((MainActivity) getActivity()).playSelectedPlaylist(position, musicList);
                });

                playlistListView.setOnItemLongClickListener((parent, view, position, id) -> {
                    removeSongDialog(position);
                    return true;
                });
            } else {
                createNoMusicText(v);
            }
        } else {
            createNoMusicText(v);
        }
    }

    private void refreshView() {
        Fragment fragment = null;
        fragment = getActivity().getSupportFragmentManager().findFragmentByTag("playlist_fragment_popup");
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.detach(fragment);
        ft.attach(fragment);
        ft.commit();
    }

    private void removeSongDialog(int position) {
        Dialog dialog = new Dialog(Objects.requireNonNull(getContext()));
        dialog.setContentView(R.layout.dialog_delete_playlist);
        dialog.show();

        TextView editText = dialog.findViewById(R.id.dialog_delete_text);
        editText.setText("Delete : " + playlistNameFromIntent + " ?");

        dialog.findViewById(R.id.dialog_delete_delete_button).setOnClickListener((View view) -> {
            removeFromPlaylist(musicList.get(position).getSongId());
            Toast.makeText(getContext(), "Song removed from playlist", Toast.LENGTH_LONG).show();
            refreshView();
            dialog.cancel();
        });
        dialog.findViewById(R.id.dialog_delete_cancel_button).setOnClickListener((View view) -> {
            dialog.cancel();
        });
    }

    private void removeFromPlaylist(int audioId) {
        ContentResolver contentResolver = getActivity().getContentResolver();
        String id = Long.toString(audioId);
        String[] whereVal = {id};
        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistIdFromIntent);
        contentResolver.delete(uri, MediaStore.Audio.Playlists.Members._ID + "=?", whereVal);
    }

    private void createNoMusicText(View view) {
        TextView noMusicText = new TextView(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);

        noMusicText.setLayoutParams(params);
        noMusicText.setText(R.string.noMusic);
        noMusicText.setTextSize(25);
        noMusicText.setTextColor(Color.DKGRAY);
        noMusicText.setGravity(Gravity.CENTER);
        ((LinearLayout) view.findViewById(R.id.linearPlaylistListLayout)).addView(noMusicText);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((MainActivity) getActivity()).hideBottomBar(false);
    }
}
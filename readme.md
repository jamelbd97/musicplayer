# Music Player
#### (Project created 17-dec-2019)
### Created by : Mohamed Jamel Bouassida (3 IM 5)

## Change Log :
##### Update (28-dec-2019) : 
+ Apearence changes.

##### Update (03-jan-2020) :
+ Added 3 Fragments : home , artists , music
+ bottom navigation bar containing 3 buttons to switch between fragments

##### Update (07-jan-2020) :
+ Added elements to the MusicFragment
+ Added a NowPlaying activity

##### Update (12-jan-2020) :
- Removed NowPlaying activity
+ Added a sliding bottom sheet containing the now playing song (now_playing.xml)
+ MusicFragment changes :
	* Added internal storage permission request
	* Added a dynamic listview generated using music files from internal storage
+ Added french language (strings.xml)

##### Update(17-jan-2020) :
+ MusicFragment changes :
	* music now plays on item click
+ Edited now playing (Sliding bottom sheet) : 
	* media control now working properly (play,prev,next)
	* music album art now chowing properly
	* added new seekbar to control wich part of the mp3 song to be played

##### Update(23-jan-2020) :
+ Added new service (MediaPlayerService) :
	* Contains and controls the media player
	* Handles media playback on incoming call
	* Pauses media on headphone removal
	* Create media playback foreground notification
+ MusicFragment changes :
	- Removed media player from MusicFragment
+ AlbumsFragment changes :
	* Contains a GridView generated from music album pictures
+ MainActivity changes :
	* NowPlaying seekbar updates with music progression

##### Update(26-jan-2020) :
+ Removed HomeFragment
+ Added PlaylistsFragment :
	* Contains list of playlists from storage
	* Add music to playlist [unstable]
+ Updated NowPlayingScreen :
	* better look with animations
	* new functions ( repeat , shuffle ) [unstable]
	
##### Update(3-feb-2020) :
+ Bug fixes :
	* Fixed playlists managements
	* Fixed albums not showing correctly
	* Fixed random play button
	
## Apk download link :
[Release 1](https://drive.google.com/file/d/1FZ_qdqgLKtvWt-LBhq3vb3gntldOODA3/view?usp=sharing) /
[Release 2](https://drive.google.com/file/d/1ac20MFCZnFZp05jvMPJLBfXzX8SqgS5D/view?usp=sharing) /
[Release 3](https://drive.google.com/file/d/1QMVvNS_EZvN4-oAo1SpueQiWB8rvqGuX/view?usp=sharing) (24-jan-2020) /
[Release 4](https://drive.google.com/file/d/1wWGeWK8H9JYQ4dGbjEJeKD6IBZB-yMV6/view?usp=sharing) (26-jan-2020) /
[Release 5](https://drive.google.com/file/d/1usRSBZAHdQQXyQabR8kM4pg-QPGnoBlT/view?usp=sharing) (3-feb-2020)

## App preview :
![](https://drive.google.com/uc?export=download&id=19lFYBjD_1NjUu-QllI2bI-3QT0ppDIAW)
![](https://drive.google.com/uc?export=download&id=110G_xK_hoSfJ9exHAEhZJ5VZAbtNqUZ8)
![](https://drive.google.com/uc?export=download&id=1sMmfhhXAkpd-c3SwOtoLP66YJOhsR3RA)

## App Video Preview :
[Video preview](https://drive.google.com/file/d/1Z7OqTlNKs7vNfhsyhXaKS-VWGOnANrpu/view?usp=sharing)
